extends Label

var format_text
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	format_text = text
	text = build_text()
	
func build_text():
	return format_text.format({
		"days": str(Global.get_day()),
		"throws": str(Global.get_throws()),
		"kos": str(Global.get_kos()),
		"misses": str(Global.get_misses()),
		"accuracy": _get_accuracy_format(),
	})

func _get_accuracy_format():
	return "%5.1f %%" % (Global.get_accuracy() * 100)
