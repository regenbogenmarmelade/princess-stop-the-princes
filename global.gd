extends Node2D

# Constants
# ---------
var LAST_DAY = 30
var SEC_PER_DAY = 30
var SEC_PER_DAY_RELOAD = 0.5

var ATTACK_FORCE_MAX = 1.0
var ATTACK_FORCE_MIN = 0.0
var ATTACK_MS_PER_FORCE = 750.0
var ATTACK_MS_DELAY = 100
var ATTACK_ARC_COLOR = Color(0.2, 0.2, 0.2)

var AMMO_MAX = 20
var AMMO_RELOAD_ARC_COLOR = ATTACK_ARC_COLOR
var AMMO_RELOAD_TIME = 3.0

var GROOM_POWER = 5

var SAVE_SETTINGS_FILE = "user://doomgroom.json"

# Mutable private things
# --------------
var _day = 1
var _throws = 0
var _kos = 0
var _misses = 0
var _health = 100
var _settings = {
	"sound": false
}

# Signals
# -------
signal day(value)
signal health(value)
signal settings(key, value)
signal settings_init() 

# Lifecycle functions
# ----------------
func _ready():
	_load_settings()

# Public functions
# ----------------
func reset():
	_day = 1
	_throws = 0
	_kos = 0
	_misses = 0

func on_groom_die():
	_kos += 1
	try_game_win()

# day --------

func get_day():
	return _day

func set_day(value):
	_day = value
	try_game_win()
	emit_signal("day", value)

func next_day():
	set_day(_day + 1)
	
# throw ------
	
func add_throw():
	_throws += 1
	
func get_throws():
	return _throws

# kos -------

func get_kos():
	return _kos
	
# misses -----

func add_miss():
	_misses += 1
	
func get_misses():
	return _misses
	
# accuracy ---

func get_accuracy():
	if _throws == 0:
		return 0
	
	return float(_throws - _misses) / float(_throws)
	

# health -----

func hurt_tower_health():
	_health = max(0, _health - GROOM_POWER)
	emit_signal("health", _health)
	try_game_lose()

func get_tower_health():
	return _health

# settings ---

func set_settings(key, value):
	_settings[key] = value
	emit_signal("settings", key, value)
	_save_settings()
	
func get_settings(key):
	return _settings[key]

func _load_settings():
	var prev_settings = _read_settings_from_file()
	for key in prev_settings:
		_settings[key] = prev_settings[key]
	
	# Emit all the keys in settings which includes default ones too
	emit_signal("settings_init")
	
func _save_settings():
	var fs = File.new()
	
	fs.open(SAVE_SETTINGS_FILE, File.WRITE)
	fs.store_line(JSON.print(_settings))
	fs.close()
	
func _read_settings_from_file():
	var fs = File.new()
	if not fs.file_exists(SAVE_SETTINGS_FILE):
		return {}
	
	fs.open(SAVE_SETTINGS_FILE, File.READ)
	
	var parse_result = JSON.parse(fs.get_line())
	
	if parse_result.error != OK:
		print("WARNING. Failed to read saved settings: ", parse_result.error_string)
		return {}
	if !(parse_result.result is Dictionary):
		return {}
	else:
		return parse_result.result

# game win ---

func try_game_lose():
	if should_game_lose():
		get_node('/root/Main').load_game_over()

func try_game_win():
	if should_game_win():
		get_node("/root/Main").load_game_win()

func should_game_lose():
	return _health <= 0

func should_game_win():
	return !should_game_lose() && _day > LAST_DAY && get_tree().get_nodes_in_group("doomgrooms").empty()
