extends CanvasLayer

func _ready():
	stop_game_ui()
	
func stop_game_ui():
	$AttackButton.visible = false
	$UnicornButton.visible = false
	$SquirrelButton.visible = false
	$RoseButton.visible = false
	$BirdButton.visible = false
	$MagicBar.visible = false
	$Clock.visible = false
	$Clock.stop()
	
func start_game_ui():
	$SkipIntroButton.visible = false
	$AttackButton.visible = true
	$Clock.visible = true
	$Clock.start()
	_start_magic()
	
func _start_magic():
	$SquirrelButton.visible = true
	$UnicornButton.visible = true
	$RoseButton.visible = true
	$BirdButton.visible = true
	$MagicBar.visible = true
	_update_magic_button_states()

func increase_magic(amount):
	$MagicBar.value += amount
	_update_magic_button_states()
	
func decrease_magic(amount):
	$MagicBar.value -= amount
	_update_magic_button_states()

func _update_magic_button_states():
	$UnicornButton.disabled = $MagicBar.value < 80
	$SquirrelButton.disabled = $MagicBar.value < 50
	$RoseButton.disabled = $MagicBar.value < 30
	$BirdButton.disabled = $MagicBar.value < 40
