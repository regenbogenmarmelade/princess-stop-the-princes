extends TextureButton

var bird_scene = preload("res://bird/bird.tscn")

func spawn_bird():
	Ui.decrease_magic(40)
	var bird = bird_scene.instance()
	var container = get_node("/root/Main/Game/MagicalContainer")
	container.add_child(bird)
