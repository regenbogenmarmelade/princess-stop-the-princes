extends TextureButton

var unicorn_scene = preload("res://unicorn/unicorn.tscn")

func spawn_unicorn():
	Ui.decrease_magic(80)
	var unicorn = unicorn_scene.instance()
	var container = get_node("/root/Main/Game/MagicalContainer")
	container.add_child(unicorn)
	var tower = get_node("/root/Main/Game/Tower")
	unicorn.global_position = tower.global_position + Vector2(100, 0)
