extends TextureButton

var rose_scene = preload("res://rose_shrub/rose_shrub.tscn")

func spawn_roses():
	Ui.decrease_magic(30)
	var rose = rose_scene.instance()
	var container = get_node("/root/Main/Game/MagicalContainer")
	container.add_child(rose)
	
	rose.global_position = Vector2(700, 900)
	#var squirrel2 = squirrel_scene.instance()
	#container.add_child(squirrel2)
	#squirrel2.global_position = Vector2(1500, 600)
	
	#var squirrel3 = squirrel_scene.instance()
	#container.add_child(squirrel3)
	#squirrel3.global_position = Vector2(550, 700)
