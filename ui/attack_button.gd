extends TextureButton

var ammo = 20
var start_ms = 0
var reload_timer = null
var should_draw = false
var center = Vector2(0,0)

func attack(force):
	if !_has_ammo():
		return
	
	_decrease_ammo()
	Throwables.create_from_princess(force)
	_stop()
	
func _ready():
	center = _calculate_center()
	_init_ammo()

func _process(_delta):
	if should_draw:
		update()
	should_draw = _is_started() || _is_reloading()

func _input(event):
	if event.is_action_pressed("Throw"):
		_start_attack()
	if event.is_action_released("Throw"):
		_release_attack()

func _start_attack():
	start_ms = OS.get_ticks_msec()
	
func _stop():
	start_ms = 0

func _is_started():
	return start_ms > 0
	
func _is_reloading():
	return reload_timer != null
	
func _release_attack():
	attack(_get_force())
	
func _get_force():
	if !_is_started():
		return 0
	
	var finish_ms = OS.get_ticks_msec()
	var force = (finish_ms - start_ms - Global.ATTACK_MS_DELAY)/Global.ATTACK_MS_PER_FORCE
	return min(Global.ATTACK_FORCE_MAX, max(Global.ATTACK_FORCE_MIN, force))

func _init_ammo():
	$Ammo/LabelCap.text = str(Global.AMMO_MAX)
	_update_ammo_label()

func _has_ammo():
	return ammo > 0
	
func _decrease_ammo():
	ammo -= 1
	_on_ammo_change()
	
func _reset_ammo():
	ammo = Global.AMMO_MAX
	_on_ammo_change()
	
func _on_ammo_change():
	if ammo <= 0:
		self.disabled = true
		_start_reloading()
		
	_update_ammo_label()

func _start_reloading():
	reload_timer = get_tree().create_timer(Global.AMMO_RELOAD_TIME)
	reload_timer.connect('timeout', self, '_finish_reloading')
	get_node("/root/Main/Game/Princess").visible = false

func _finish_reloading():
	reload_timer = null
	_reset_ammo()
	self.disabled = false
	get_node("/root/Main/Game/Princess").visible = true

func _update_ammo_label():
	$Ammo/LabelRemaining.text = str(ammo)

# DRAW FUNCTIONS
# --------------
func _draw():
	if _is_started():
		_draw_force_arc()
	elif _is_reloading():
		_draw_reloading_arc()
		
func _draw_force_arc():
	var ratio = _get_force()
	var angle = 2 * PI * ratio
	draw_arc(center, center.x, 0, angle, 256, Global.ATTACK_ARC_COLOR, 5, true)
	
func _draw_reloading_arc():
	var ratio = -reload_timer.time_left / Global.AMMO_RELOAD_TIME
	var angle = 2 * PI * ratio
	draw_arc(center, center.x, 0, angle, 256, Global.AMMO_RELOAD_ARC_COLOR, 5, true)
	
# HELPERS
# -------
func _calculate_center():
	var width = self.texture_normal.get_width()
	var mid = width / 2
	return Vector2(mid, mid)

