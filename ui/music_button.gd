extends TextureButton

func _ready():
	Global.connect("settings", self, "_on_settings_change")
	Global.connect("settings_init", self, "_on_settings_init")
	
func _is_on():
	return bool(Global.get_settings("sound"))

func toggle_music():
	var val = !_is_on()
	Global.set_settings("sound", val)

func _on_settings_change(key, value):
	if key == "sound":
		_on_sound_change(value)
		
func _on_settings_init():
	_on_sound_change(_is_on())

func _on_sound_change(is_on):
	$Sprite.visible = is_on
	
	var master_bus_index = AudioServer.get_bus_index("Master")
	var is_muted = !AudioServer.is_bus_mute(master_bus_index)
	
	AudioServer.set_bus_mute(master_bus_index, !is_on)
	if !is_muted:
		var music = get_node("/root/Main/Music")
		if music:
			music.seek(0)
