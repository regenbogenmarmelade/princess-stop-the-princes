extends TextureButton

var squirrel_scene = preload("res://squirrel/squirrel.tscn")

func spawn_squirrel():
	Ui.decrease_magic(50)
	var squirrel = squirrel_scene.instance()
	var container = get_node("/root/Main/Game/MagicalContainer")
	container.add_child(squirrel)
	squirrel.global_position = Vector2(950, 500)
	
	var squirrel2 = squirrel_scene.instance()
	container.add_child(squirrel2)
	squirrel2.global_position = Vector2(1500, 600)
	
	var squirrel3 = squirrel_scene.instance()
	container.add_child(squirrel3)
	squirrel3.global_position = Vector2(550, 700)
