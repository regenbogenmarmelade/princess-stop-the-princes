extends Node2D

func _ready():
	$Timer.wait_time = Global.SEC_PER_DAY
	$ReloadTimer.wait_time = Global.SEC_PER_DAY_RELOAD
	Global.connect("day", self, "_on_day_change")

func start():
	$Timer.start()
	$SunPath.visible = true
	
func stop():
	$Timer.stop()
	$ReloadTimer.stop()
	
func start_reload():
	$ReloadTimer.start()
	$SunPath.visible = false

func _process(_delta):
	if !$Timer.is_stopped():
		_update_sun_position()

func _update_sun_position():
	$SunPath/SunPathFollow.unit_offset = 1 - $Timer.time_left / $Timer.wait_time

func _on_Timer_timeout():
	Global.next_day()

func _on_ReloadTimer_timeout():
	start()

func _on_day_change(day):
	if day <= Global.LAST_DAY:
		$LabelDay.text = str(day)
		start_reload()
