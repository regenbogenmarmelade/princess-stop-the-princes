extends Node2D

var roseHealth = 100
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$RigidBody2D/AnimatedSprite.frame = 0
	$RigidBody2D/AnimatedSprite2.frame = 0
	$RigidBody2D/AnimatedSprite.connect("animation_finished", self, "_on_AnimatedSprite_animation_finished")
	$RigidBody2D/AnimatedSprite.play("default")
	
	$RigidBody2D/AnimatedSprite2.connect("animation_finished", self, "_on_AnimatedSprite_animation_finished")
	$RigidBody2D/AnimatedSprite2.play("default")
	
	
func _on_AnimatedSprite_animation_finished():
	if $RigidBody2D/AnimatedSprite.animation == "default":
		$RigidBody2D/AnimatedSprite.stop()
		$RigidBody2D/AnimatedSprite.frame = 6
		
		$RigidBody2D/AnimatedSprite2.stop()
		$RigidBody2D/AnimatedSprite2.frame = 5


func check_attack():
	var has_groom_attacker = false

	for groom in get_tree().get_nodes_in_group("doomgrooms"):
		if $RigidBody2D/Area2D.overlaps_body(groom):
			groom.attacking = true
			has_groom_attacker = true
			
			if(roseHealth == 0) :
				groom.attacking = false
				queue_free()

	if has_groom_attacker:
		#$KnockKnock.play()
		roseHealth = roseHealth - 25
		$RigidBody2D/Health.value = roseHealth
	
	
