extends RigidBody2D

export var speed = 25;
var direction = 1

func _physics_process(_delta):
	move()
	
	if position.x > 1600:
		$Music.volume_db = (1600 - position.x) / 10

func move():
	 apply_central_impulse(Vector2(500, 0))

func die():
	queue_free()


func push():
	linear_velocity.x = 500
	#apply_central_impulse(Vector2(500, 0))
