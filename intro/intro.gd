extends Node2D

var music_files = [
	load("res://music/slide-1.ogg"),
	load("res://music/slide-2.ogg"),
	load("res://music/slide-3.ogg"),
	load("res://music/slide-4.ogg"),
	load("res://music/slide-5.ogg"),
	load("res://music/slide-6.ogg"),
	load("res://music/bard.ogg"),
]

func _make_recursively_unmousable(node):
	for child in node.get_children():
		if child.is_class("Control"):
			child.mouse_filter = Control.MOUSE_FILTER_IGNORE
		_make_recursively_unmousable(child)

func _ready():
	_make_recursively_unmousable(self)
	$bard3/Text.text = $bard3/Text.text.format({ "days": Global.LAST_DAY })

func _unhandled_input(event):
	if event.is_action_pressed("ui_accept"):
		next_slide()

func next_slide():
	var is_last_slide = get_child_count() == 1
	get_children().back().queue_free()
	
	if is_last_slide:
		get_node('/root/Main').load_game()
	else:
		var music_stream = music_files.pop_front()
		music_stream.set_loop(true)
		
		var music = get_node("/root/Main/Music") as AudioStreamPlayer
		var position = music.get_playback_position()
		music.set_stream(music_stream)
		music.play(position)
