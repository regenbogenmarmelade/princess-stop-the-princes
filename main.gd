extends YSort

export(AudioStream) var game_over_music
export(AudioStream) var main_music

var GAME_SCENE = preload("res://game/game.tscn")
var GAME_OVER_SCENE = preload("res://game_over/game_over.tscn")
var GAME_WIN_SCENE = preload("res://game_over/game_win.tscn")

func __ready():
	Global.reset()

func load_game():
	_load_scene(GAME_SCENE)

func load_game_over():
	_load_scene(GAME_OVER_SCENE, game_over_music)
	
func load_game_win():
	_load_scene(GAME_WIN_SCENE)

func _load_scene(scene, new_music = main_music):
	for child in get_children():
		if !child.is_in_group("keepalive"):
			child.queue_free()
	
	add_child(scene.instance())
	
	if $Music.stream != new_music:
		$Music.stop()
		$Music.set_stream(new_music)
		$Music.play()
