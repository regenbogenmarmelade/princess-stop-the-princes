extends YSort

func kill(body : Node):
	# do not kill the ground, it is innocent
	if (body.get_parent() == self):
		return

	body.queue_free()
