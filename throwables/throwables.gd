extends Node2D

var MAX_IMPULSE = 1400
var DEFAULT_GRAVITY_SCALE = 20
var MAX_ANGULAR_VELOCITY = 30

func _init():
	visible = false

func create_from_princess(force):
	# Not hard coding "Main" in here helps us run the Game scene in isolation
	var game = get_tree().get_nodes_in_group("game")[0]
	Global.add_throw()

	var random_index = randi() % get_child_count()
	var throwable = get_child(random_index).duplicate() as RigidBody2D
	throwable.sleeping = false
	throwable.from_princess = true
	game.add_child(throwable)
	
	# Some objects are heavier than others so we adjust the impulse
	# so it can still reach to the end
	var scale = throwable.gravity_scale / DEFAULT_GRAVITY_SCALE
	var scale_adj = sqrt(scale)
	var impulse = Vector2(MAX_IMPULSE * scale_adj * force, 0)

	var princess = game.get_node("Princess")
	throwable.global_position = princess.global_position
	throwable.global_position.x += 60
	throwable.global_position.y -= 40
	throwable.angular_velocity = force * MAX_ANGULAR_VELOCITY / scale
	
	# throwable.apply_impulse(princess.global_position, impulse)
	throwable.apply_central_impulse(impulse)
	
	var sounds = princess.get_node("Sounds")
	var sound = sounds.get_child(randi() % sounds.get_child_count())
	sound.play()

func create_nuts(force, pos):
	var game = get_tree().get_nodes_in_group("game")[0]
	
	var throwable = get_child(1).duplicate() as RigidBody2D
	throwable.sleeping = false
	game.add_child(throwable)
	
	
	
	# Some objects are heavier than others so we adjust the impulse
	# so it can still reach to the end
	#var scale = throwable.gravity_scale / DEFAULT_GRAVITY_SCALE
	#var scale_adj = sqrt(scale)
	#var impulse = Vector2(MAX_IMPULSE * scale_adj * force, 0)
	throwable.global_position = pos    #squirrel.global_position
	throwable.global_position.x -= 15
	throwable.global_position.y += 30
	
