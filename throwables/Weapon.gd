extends RigidBody2D

var active = true
var is_miss = true
var from_princess = false

func _on_body_shape_entered(_body_id, body, _body_shape, _local_shape):
	if (!self.active):
		return
		
	if (body.has_method('hit')):
		is_miss = false
		body.hit(from_princess)
	
	if (body.name == "GroundNode" && $Timer.is_stopped()):
		on_ground_hit()

func _on_Timer_timeout():
	die()
	
func on_ground_hit():
	if from_princess && is_miss:
		Global.add_miss()
	
	deactivate()
	self.sleeping = true
	$Timer.start()
	self.collision_layer = 0
	self.collision_mask = 0
	
# decativate()
# Collisions will no longer be considered hits. This way a weapon can lay around but not cause damage.
func deactivate():
	self.active = false
	
	var sound = get_node("Sound")
	if sound:
		sound.play()
	
	if(self.name == "Watermelon" || self.name == "GreenChemical" || self.name == "Cactus"): 
		$Sprite.visible = false
		$Sprite2.visible = true
		
		if(self.name == "GreenChemical") :
			$Particle1.visible = true
	
				

func die():
	
	if(self.name == "Chest") :
		var probability = 0
		probability = rand_range(0,10)
		
		if(probability > 6) :
			var chest_scene = preload("res://magic_chest/magic_chest.tscn")
			var magic_chest = chest_scene.instance()
			var container = get_node("/root/Main/Game/MagicalContainer")
			container.add_child(magic_chest)
	
			magic_chest.global_position = self.global_position
	queue_free()
