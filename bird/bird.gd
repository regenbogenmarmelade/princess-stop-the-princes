extends Node2D

var t = 0.0
var reachTarget = false 

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	
	if(reachTarget) :
		queue_free()
		return
	
	t += delta * 0.3
	
	$Sprite.global_position = $Start1.global_position.linear_interpolate($Target.global_position, t)
	$Sprite2.global_position = $Start2.global_position.linear_interpolate($Target.global_position, t)
	
	if($Sprite.position.x > $Target.position.x - 10) :
		reachTarget = true
	
	

func attack1():
	Throwables.create_nuts(10, $Sprite.global_position)


func attack2():
	Throwables.create_nuts(10, $Sprite2.global_position)
