extends Node2D

func _ready():
	Global.connect("health", self, "_show_health")
	_show_health(Global.get_tower_health())
	
func _show_health(val):
	$HealthBar.value = val

func check_attack():
	var has_groom_attacker = false

	for groom in get_tree().get_nodes_in_group("doomgrooms"):
		if $Area2D.overlaps_body(groom):
			groom.attacking = true
			has_groom_attacker = true

	if has_groom_attacker:
		$KnockKnock.play()
		Global.hurt_tower_health()

