extends YSort

var princes_spawned = 0
var spawn_timer = 3.0
var min_spawn_timer = 2.0
var cluster_length = 0
var cluster_spawn_time = 0.5
var rng = RandomNumberGenerator.new()
var cluster_cooldown = 0

func _ready():
	start_spawning()

func spawn():
	if (Global.get_day() > Global.LAST_DAY):
		return
	update_spawn_time()
	var lancelot_scene = preload("res://doomgrooms/groom_lancelot.tscn")
	var lancelot = lancelot_scene.instance()
	lancelot.add_to_group("doomgrooms")
	add_child(lancelot)
	princes_spawned += 1
	if (cluster_length > 0):
		cluster_length -= 1
		if (cluster_length < 1):
			cluster_cooldown = 5	
		
func update_spawn_time():
	if (cluster_length > 0):
		spawn_timer = cluster_spawn_time
	else:
		spawn_timer = 4.0 - 0.2 * Global.get_day()
		if (spawn_timer < min_spawn_timer):
			spawn_timer = min_spawn_timer
		cluster()
	start_spawning()
	
func cluster():
	if (cluster_cooldown > 0):
		cluster_cooldown -= 1
		return
	if (rng.randi_range(0,3) == 1):
		var max_cluster = 7
		cluster_length = rng.randi_range(3,max_cluster)
		cluster_spawn_time = rng.randf_range(0.3, 0.5)
			
		

func start_spawning():
	$Spawntimer.start(spawn_timer)
	
func stop_spawning():
	$Spawntimer.stop()


func trainSpawn():
	
	var train_scene = preload("res://train/train.tscn")
	var train = train_scene.instance()
	add_child(train)
	
	
