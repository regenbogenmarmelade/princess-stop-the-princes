extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var t = 0.0

var reachedStation = false
var continueJourney = false
var reachedTarget = false
var noOfPrinces = 0 


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _physics_process(delta):
	t += delta * 0.4
	if($Sprite.position.x < $Station.position.x && !continueJourney) :
		reachedStation = true
		
		if(noOfPrinces < 3) :
			var lancelot_scene = preload("res://doomgrooms/groom_lancelot.tscn")
			var lancelot = lancelot_scene.instance()
			lancelot.add_to_group("doomgrooms")
			add_child(lancelot)
			lancelot.global_position = $Sprite/Door.global_position
			
			var lancelot2 = lancelot_scene.instance()
			lancelot2.add_to_group("doomgrooms")
			add_child(lancelot2)
			lancelot2.global_position = $Sprite/Door2.global_position
			
			var lancelot3 = lancelot_scene.instance()
			lancelot3.add_to_group("doomgrooms")
			add_child(lancelot3)
			lancelot3.global_position = $Sprite/Door3.global_position

			noOfPrinces = noOfPrinces+1
	
	if(!reachedStation && !continueJourney) :
		go_to_station()
		$StationTime.start(1)
		
	if(continueJourney && reachedStation) :
		go_to_target()

func go_on():
	continueJourney = true
	t = 0.0
	
func go_to_station() :
	$Sprite.global_position = $Start.global_position.linear_interpolate($Station.global_position, t)

func go_to_target() :
	
	$Sprite.position = $Station.position.linear_interpolate($Target.position, t)
	
	if($Sprite.position.x < $Target.position.x) :
		reachedTarget = true
	
