extends RigidBody2D

export var speed = 100
export var health = 1

var attacking = false
var is_about_to_die = false

func _ready():
	self.connect("tree_exited", Global, "on_groom_die")

func _physics_process(_delta):
	move()

func move():
	if is_about_to_die:
		linear_velocity = Vector2(0, 0)
		#$AnimatedSprite.stop()
		#$AnimatedSprite.frame = 0
		$AnimatedSprite.play("hit")
		return

	linear_velocity.x = -speed
	
	if attacking:
		$AnimatedSprite.play("attacking")
	else :
		$AnimatedSprite.play("default")

func die():
	is_about_to_die = true
	sleeping = true
	
	$AnimatedSprite.play("hit")	

	var sound_index = randi() % $Sounds.get_child_count()
	var sound = $Sounds.get_child(sound_index)
	
	sound.play()
	sound.connect("finished", self, "queue_free")

func hit(from_princess = false):
	if is_about_to_die:
		return
	if from_princess:
		Ui.increase_magic(5)

	health -= 1
	
	if health <= 0:
		die()
